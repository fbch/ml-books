# ML-Books
Machine Learning Resources

[ISL 2nd](https://www.statlearning.com/)

[ESL 2nd](https://hastie.su.domains/ElemStatLearn/)

[ITILA](https://www.inference.org.uk/itila/book.html)

[MLAPP](https://doc.lagout.org/science/Artificial%20Intelligence/Machine%20learning/Machine%20Learning_%20A%20Probabilistic%20Perspective%20%5BMurphy%202012-08-24%5D.pdf)

[PRML](https://www.microsoft.com/en-us/research/people/cmbishop/prml-book/)

[Understanding Machine Learning: From Theory to Algorithms](https://www.cs.huji.ac.il/~shais/UnderstandingMachineLearning/copy.html)

[Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow 2nd](https://www.knowledgeisle.com/wp-content/uploads/2019/12/2-Aur%c3%a9lien-G%c3%a9ron-Hands-On-Machine-Learning-with-Scikit-Learn-Keras-and-Tensorflow_-Concepts-Tools-and-Techniques-to-Build-Intelligent-Systems-O%e2%80%99Reilly-Media-2019.pdf)

[Hands-On Machine Learning with Scikit-Learn, Keras, and TensorFlow 2nd CN](https://github.com/apachecn/hands-on-ml-2e-zh)

[Deep Learning Book](https://github.com/janishar/mit-deep-learning-book-pdf)

[Deep Learning Book CN](https://github.com/exacity/deeplearningbook-chinese)

[Reinforcement Learning: An Introduction 2nd](http://incompleteideas.net/book/the-book-2nd.html)

[Foundations of Reinforcement Learning with Applications in Finance](https://github.com/TikhonJelvis/RL-book)
